#![feature(trait_alias)]
#![feature(more_qualified_paths)]
#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::ops;

fn superscript(n: i64) -> String {
    const SUPERSCRIPTS: [&str; 10] = ["⁰", "¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹"];
    let mut result = String::new();
    for c in n.to_string().chars() {
        if c == '-' {
            result += "⁻"
        } else {
            result += SUPERSCRIPTS.get(c.to_digit(10).unwrap() as usize).unwrap();
        }
    }
    result
}

#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Value<
    const METRE: i64,
    const KILOGRAM: i64,
    const SECOND: i64,
    const AMPERE: i64,
    const KELVIN: i64,
    const MOLE: i64,
    const CANDELA: i64,
> {
    value: f64,
}

impl<
        const METRE: i64,
        const KILOGRAM: i64,
        const SECOND: i64,
        const AMPERE: i64,
        const KELVIN: i64,
        const MOLE: i64,
        const CANDELA: i64,
    > std::fmt::Display for Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut unit_strings_vec = Vec::new();
        let our_id: std::any::TypeId = std::any::TypeId::of::<Self>();

        macro_rules! codegen_if_block_unit_string {
            ($(($T:ty, $s:literal)),*) => {
                $(
                    if our_id == std::any::TypeId::of::<$T>() {
                        unit_strings_vec.push((1, $s));
                    }
                )*
            }
        }

        codegen_if_block_unit_string!(
            (Hertz, "Hz"),
            (Radian, "rad"),
            (Steradian, "sr"),
            (Newton, "N"),
            (Pascal, "Pa"),
            (Joule, "J"),
            (Watt, "W"),
            (Coulomb, "C"),
            (Volt, "V"),
            (Farad, "F"),
            (Ohm, "Ω"),
            (Siemens, "S"),
            (Weber, "Wb"),
            (Tesla, "T"),
            (Henry, "H"),
            (Lumen, "lm"),
            (Lux, "lx"),
            (Becquerel, "Bq"),
            (Gray, "Gy"),
            (Sievert, "Sv"),
            (Katal, "kat"),
            (MetrePerSecond, "m/s"),
            (MetrePerSecondSquared, "m/s²"),
            (KilogramPerSquareMetre, "kg/m²"),
            (KilogramPerCubicMetre, "kg/m³"),
            (CubicMetrePerKilogram, "m³/kg"),
            (AmperePerSquareMetre, "A/m²"),
            (AmperePerMetre, "A/m"),
            (MolePerCubicMetre, "mol/m³"),
            (CandelaPerSquareMetre, "cd/m²"),
            (CubicMetrePerMole, "m³/mol"),
            (KilogramPerMole, "kg/mol"),
            (PascalSecond, "Pa·s"),
            (JouleSecond, "J·s"),
            (NewtonPerMetre, "N/m"),
            (RadianPerSecond, "rad/s"),
            (RadianPerSecondSquared, "rad/s²"),
            (WattPerSquareMetre, "W/m²"),
            (JoulePerKelvin, "J/K"),
            (JoulePerKilogramKelvin, "J/(kg·K)"),
            (JoulePerKilogram, "J/kg"),
            (WattPerMetreKelvin, "W/(m·K)"),
            (JoulePerCubicMetre, "J/m³"),
            (VoltPerMetre, "V/m"),
            (CoulombPerCubicMetre, "C/m³"),
            (CoulombPerSquareMetre, "C/m²"),
            (FaradPerMetre, "F/m"),
            (HenryPerMetre, "H/m"),
            (JoulePerMole, "J/mol"),
            (JoulePerMoleKelvin, "J/(mol·K)"),
            (CoulombPerKilogram, "C/kg"),
            (GrayPerSecond, "Gy/s"),
            (WattPerSteradian, "W/sr"),
            (WattPerSquareMetreSteradian, "W/(m²·sr)"),
            (KatalPerCubicMetre, "kat/m³"),
            (KelvinPerWatt, "K/W"),
            (SiemensPerMetre, "S/m"),
            (OhmMetre, "Ω·m"),
            (LumenSecond, "lm·s"),
            (LuxSecond, "lx·s"),
            (LumenSecondPerCubicMetre, "lm·s/m³"),
            (LumenPerWatt, "lm/W")
        );

        if unit_strings_vec.len() > 1 {
            unit_strings_vec.clear();
        }

        if unit_strings_vec.len() == 0 {
            for &(i, s) in &[
                (METRE, "m"),
                (KILOGRAM, "kg"),
                (SECOND, "s"),
                (AMPERE, "A"),
                (KELVIN, "K"),
                (MOLE, "mol"),
                (CANDELA, "cd"),
            ] {
                if i != 0 {
                    unit_strings_vec.push((i, s));
                }
            }
        }

        let mut unit_string = String::new();
        for (index, &(i, s)) in unit_strings_vec.iter().enumerate() {
            assert!(i != 0);

            unit_string += s;
            if i != 1 {
                unit_string += &superscript(i);
            } else if index != unit_strings_vec.len() - 1 {
                unit_string += &"·";
            }
        }
        if f64::max(1.0 / self.value, self.value) <= 10_000.0 {
            let v = (self.value * 10_000.0) as i64 as f64 / 10_000.0;
            write!(f, "{} {}", v, &unit_string)
        } else {
            write!(f, "{:.4e} {}", self.value, &unit_string)
        }
    }
}

fn get_dims_string<
    const METRE: i64,
    const KILOGRAM: i64,
    const SECOND: i64,
    const AMPERE: i64,
    const KELVIN: i64,
    const MOLE: i64,
    const CANDELA: i64,
>(
    _: Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>,
) -> String {
    format!("Value<{METRE}, {KILOGRAM}, {SECOND}, {AMPERE}, {KELVIN}, {MOLE}, {CANDELA}>")
}

impl<
        const METRE: i64,
        const KILOGRAM: i64,
        const SECOND: i64,
        const AMPERE: i64,
        const KELVIN: i64,
        const MOLE: i64,
        const CANDELA: i64,
    > ops::Add<Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>>
    for Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>
{
    type Output = Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>;

    fn add(self, rhs: Self::Output) -> Self::Output {
        Self::Output {
            value: self.value + rhs.value,
        }
    }
}

impl<
        const METRE: i64,
        const KILOGRAM: i64,
        const SECOND: i64,
        const AMPERE: i64,
        const KELVIN: i64,
        const MOLE: i64,
        const CANDELA: i64,
    > ops::Sub<Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>>
    for Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>
{
    type Output = Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>;

    fn sub(self, rhs: Self::Output) -> Self::Output {
        Self::Output {
            value: self.value - rhs.value,
        }
    }
}

impl<
        const METRE: i64,
        const KILOGRAM: i64,
        const SECOND: i64,
        const AMPERE: i64,
        const KELVIN: i64,
        const MOLE: i64,
        const CANDELA: i64,
    > ops::Neg for Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>
{
    type Output = Value<METRE, KILOGRAM, SECOND, AMPERE, KELVIN, MOLE, CANDELA>;

    fn neg(self) -> Self::Output {
        Self::Output { value: -self.value }
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
        const RHS_METRE: i64,
        const RHS_KILOGRAM: i64,
        const RHS_SECOND: i64,
        const RHS_AMPERE: i64,
        const RHS_KELVIN: i64,
        const RHS_MOLE: i64,
        const RHS_CANDELA: i64,
    >
    ops::Mul<
        Value<RHS_METRE, RHS_KILOGRAM, RHS_SECOND, RHS_AMPERE, RHS_KELVIN, RHS_MOLE, RHS_CANDELA>,
    >
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
where
    // Why do I need this constraint? I don't know, I copied the solution from here: https://stackoverflow.com/a/66362584/6860744
    Value<
        { SELF_METRE + RHS_METRE },
        { SELF_KILOGRAM + RHS_KILOGRAM },
        { SELF_SECOND + RHS_SECOND },
        { SELF_AMPERE + RHS_AMPERE },
        { SELF_KELVIN + RHS_KELVIN },
        { SELF_MOLE + RHS_MOLE },
        { SELF_CANDELA + RHS_CANDELA },
    >: Sized,
{
    type Output = Value<
        { SELF_METRE + RHS_METRE },
        { SELF_KILOGRAM + RHS_KILOGRAM },
        { SELF_SECOND + RHS_SECOND },
        { SELF_AMPERE + RHS_AMPERE },
        { SELF_KELVIN + RHS_KELVIN },
        { SELF_MOLE + RHS_MOLE },
        { SELF_CANDELA + RHS_CANDELA },
    >;

    fn mul(
        self,
        rhs: Value<
            RHS_METRE,
            RHS_KILOGRAM,
            RHS_SECOND,
            RHS_AMPERE,
            RHS_KELVIN,
            RHS_MOLE,
            RHS_CANDELA,
        >,
    ) -> Self::Output {
        Self::Output {
            value: self.value * rhs.value,
        }
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
        const RHS_METRE: i64,
        const RHS_KILOGRAM: i64,
        const RHS_SECOND: i64,
        const RHS_AMPERE: i64,
        const RHS_KELVIN: i64,
        const RHS_MOLE: i64,
        const RHS_CANDELA: i64,
    >
    ops::Div<
        Value<RHS_METRE, RHS_KILOGRAM, RHS_SECOND, RHS_AMPERE, RHS_KELVIN, RHS_MOLE, RHS_CANDELA>,
    >
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
where
    Value<
        { SELF_METRE - RHS_METRE },
        { SELF_KILOGRAM - RHS_KILOGRAM },
        { SELF_SECOND - RHS_SECOND },
        { SELF_AMPERE - RHS_AMPERE },
        { SELF_KELVIN - RHS_KELVIN },
        { SELF_MOLE - RHS_MOLE },
        { SELF_CANDELA - RHS_CANDELA },
    >: Sized,
{
    type Output = Value<
        { SELF_METRE - RHS_METRE },
        { SELF_KILOGRAM - RHS_KILOGRAM },
        { SELF_SECOND - RHS_SECOND },
        { SELF_AMPERE - RHS_AMPERE },
        { SELF_KELVIN - RHS_KELVIN },
        { SELF_MOLE - RHS_MOLE },
        { SELF_CANDELA - RHS_CANDELA },
    >;

    fn div(
        self,
        rhs: Value<
            RHS_METRE,
            RHS_KILOGRAM,
            RHS_SECOND,
            RHS_AMPERE,
            RHS_KELVIN,
            RHS_MOLE,
            RHS_CANDELA,
        >,
    ) -> Self::Output {
        Self::Output {
            value: self.value / rhs.value,
        }
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::Mul<f64>
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    type Output = Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::Output {
            value: self.value * rhs,
        }
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::Div<f64>
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    type Output = Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >;

    fn div(self, rhs: f64) -> Self::Output {
        Self::Output {
            value: self.value / rhs,
        }
    }
}

impl<
        const RHS_METRE: i64,
        const RHS_KILOGRAM: i64,
        const RHS_SECOND: i64,
        const RHS_AMPERE: i64,
        const RHS_KELVIN: i64,
        const RHS_MOLE: i64,
        const RHS_CANDELA: i64,
    >
    ops::Mul<
        Value<RHS_METRE, RHS_KILOGRAM, RHS_SECOND, RHS_AMPERE, RHS_KELVIN, RHS_MOLE, RHS_CANDELA>,
    > for f64
{
    type Output =
        Value<RHS_METRE, RHS_KILOGRAM, RHS_SECOND, RHS_AMPERE, RHS_KELVIN, RHS_MOLE, RHS_CANDELA>;

    fn mul(self, rhs: Self::Output) -> Self::Output {
        Self::Output {
            value: self * rhs.value,
        }
    }
}

impl<
        const RHS_METRE: i64,
        const RHS_KILOGRAM: i64,
        const RHS_SECOND: i64,
        const RHS_AMPERE: i64,
        const RHS_KELVIN: i64,
        const RHS_MOLE: i64,
        const RHS_CANDELA: i64,
    >
    ops::Div<
        Value<RHS_METRE, RHS_KILOGRAM, RHS_SECOND, RHS_AMPERE, RHS_KELVIN, RHS_MOLE, RHS_CANDELA>,
    > for f64
where
    Value<
        { -RHS_METRE },
        { -RHS_KILOGRAM },
        { -RHS_SECOND },
        { -RHS_AMPERE },
        { -RHS_KELVIN },
        { -RHS_MOLE },
        { -RHS_CANDELA },
    >: Sized,
{
    type Output = Value<
        { -RHS_METRE },
        { -RHS_KILOGRAM },
        { -RHS_SECOND },
        { -RHS_AMPERE },
        { -RHS_KELVIN },
        { -RHS_MOLE },
        { -RHS_CANDELA },
    >;

    fn div(
        self,
        rhs: Value<
            RHS_METRE,
            RHS_KILOGRAM,
            RHS_SECOND,
            RHS_AMPERE,
            RHS_KELVIN,
            RHS_MOLE,
            RHS_CANDELA,
        >,
    ) -> Self::Output {
        Self::Output {
            value: self / rhs.value,
        }
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::AddAssign
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    fn add_assign(&mut self, other: Self) {
        self.value += other.value;
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::SubAssign
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    fn sub_assign(&mut self, other: Self) {
        self.value -= other.value;
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::MulAssign<f64>
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    fn mul_assign(&mut self, other: f64) {
        self.value *= other;
    }
}

impl<
        const SELF_METRE: i64,
        const SELF_KILOGRAM: i64,
        const SELF_SECOND: i64,
        const SELF_AMPERE: i64,
        const SELF_KELVIN: i64,
        const SELF_MOLE: i64,
        const SELF_CANDELA: i64,
    > ops::DivAssign<f64>
    for Value<
        SELF_METRE,
        SELF_KILOGRAM,
        SELF_SECOND,
        SELF_AMPERE,
        SELF_KELVIN,
        SELF_MOLE,
        SELF_CANDELA,
    >
{
    fn div_assign(&mut self, other: f64) {
        self.value /= other;
    }
}

pub type Scalar = Value<0, 0, 0, 0, 0, 0, 0>;
pub type Metre = Value<1, 0, 0, 0, 0, 0, 0>;
pub type Kilogram = Value<0, 1, 0, 0, 0, 0, 0>;
pub type Second = Value<0, 0, 1, 0, 0, 0, 0>;
pub type Ampere = Value<0, 0, 0, 1, 0, 0, 0>;
pub type Kelvin = Value<0, 0, 0, 0, 1, 0, 0>;
pub type Mole = Value<0, 0, 0, 0, 0, 1, 0>;
pub type Candela = Value<0, 0, 0, 0, 0, 0, 1>;
pub type Hertz = Value<0, 0, -1, 0, 0, 0, 0>; // s^(-1)
pub type Radian = Value<0, 0, 0, 0, 0, 0, 0>; // 1
pub type Steradian = Value<0, 0, 0, 0, 0, 0, 0>; // 1
pub type Newton = Value<1, 1, -2, 0, 0, 0, 0>; // kg*m*s^(-2)
pub type Pascal = Value<-1, 1, -2, 0, 0, 0, 0>; // kg*m^(-1)*s^(-2)
pub type Joule = Value<2, 1, -2, 0, 0, 0, 0>; // kg*m^2*s^(-2)
pub type Watt = Value<2, 1, -3, 0, 0, 0, 0>; // kg*m^2*s^(-3)
pub type Coulomb = Value<0, 0, 1, 1, 0, 0, 0>; // s*A
pub type Volt = Value<2, 1, -3, -1, 0, 0, 0>; // kg*m^2*s^(-3)*A^(-1)
pub type Farad = Value<-2, -1, 4, 2, 0, 0, 0>; // kg^(-1)*m^(-2)*s^4*A^2
pub type Ohm = Value<2, 1, -3, -2, 0, 0, 0>; // kg*m^2*s^(-3)*A^(-2)
pub type Siemens = Value<-2, -1, 3, 2, 0, 0, 0>; // kg^(-1)*m^(-2)*s^3*A^2
pub type Weber = Value<2, 1, -2, -1, 0, 0, 0>; // kg*m^2*s^(-2)*A^(-1)
pub type Tesla = Value<0, 1, -2, -1, 0, 0, 0>; // kg*s^(-2)*A^(-1)
pub type Henry = Value<2, 1, -2, -2, 0, 0, 0>; // kg*m^2*s^(-2)*A^(-2)
pub type Lumen = Value<0, 0, 0, 0, 0, 0, 1>; // cd
pub type Lux = Value<-2, 0, 0, 0, 0, 0, 1>; // cd*m^(-2)
pub type Becquerel = Value<0, 0, -1, 0, 0, 0, 0>; // s^(-1)
pub type Gray = Value<2, 0, -2, 0, 0, 0, 0>; // m^2*s^(-2)
pub type Sievert = Value<2, 0, -2, 0, 0, 0, 0>; // m^2*s^(-2)
pub type Katal = Value<0, 0, -1, 0, 0, 1, 0>; // s^(-1)*mol

pub type SquareMetre = Value<2, 0, 0, 0, 0, 0, 0>; // m^2
pub type CubicMetre = Value<3, 0, 0, 0, 0, 0, 0>; // m^3
pub type MetrePerSecond = Value<1, 0, -1, 0, 0, 0, 0>; // m/s
pub type MetrePerSecondSquared = Value<1, 0, -2, 0, 0, 0, 0>; // m/s^2
pub type ReciprocalMetre = Value<-1, 0, 0, 0, 0, 0, 0>; // m^(-1)
pub type KilogramPerSquareMetre = Value<-2, 1, 0, 0, 0, 0, 0>; // kg/m^2
pub type KilogramPerCubicMetre = Value<-3, 1, 0, 0, 0, 0, 0>; // kg/m^3
pub type CubicMetrePerKilogram = Value<3, -1, 0, 0, 0, 0, 0>; // m^3/kg
pub type AmperePerSquareMetre = Value<-2, 0, 0, 1, 0, 0, 0>; // A/m^2
pub type AmperePerMetre = Value<-1, 0, 0, 1, 0, 0, 0>; // A/m
pub type MolePerCubicMetre = Value<-3, 0, 0, 0, 0, 1, 0>; // mol/m^3
pub type CandelaPerSquareMetre = Value<-2, 0, 0, 0, 0, 0, 1>; // cd/m^2
pub type CubicMetrePerMole = Value<3, 0, 0, 0, 0, -1, 0>; // m^3/mol
pub type KilogramPerMole = Value<0, 1, 0, 0, 0, -1, 0>; // kg/mol
pub type ReciprocalMole = Value<0, 0, 0, 0, 0, -1, 0>; // mol^(-1)

pub type PascalSecond = Value<-1, 1, -1, 0, 0, 0, 0>; // Pa·s
pub type NewtonMetre = Value<2, 1, -2, 0, 0, 0, 0>; // N·m
pub type NewtonMetreSecond = Value<2, 1, -1, 0, 0, 0, 0>; // N·m·s
pub type JouleSecond = Value<2, 1, -1, 0, 0, 0, 0>; // J·s
pub type NewtonPerMetre = Value<0, 1, -2, 0, 0, 0, 0>; // N/m
pub type RadianPerSecond = Value<0, 0, -1, 0, 0, 0, 0>; // rad/s
pub type RadianPerSecondSquared = Value<0, 0, -2, 0, 0, 0, 0>; // rad/s2
pub type WattPerSquareMetre = Value<0, 1, -3, 0, 0, 0, 0>; // W/m2
pub type JoulePerKelvin = Value<2, 1, -2, 0, -1, 0, 0>; // J/K
pub type JoulePerKilogramKelvin = Value<2, 0, -2, 0, -1, 0, 0>; // J/(kg·K)
pub type JoulePerKilogram = Value<2, 0, -2, 0, 0, 0, 0>; // J/kg
pub type WattPerMetreKelvin = Value<1, 1, -3, 0, -1, 0, 0>; // W/(m·K)
pub type JoulePerCubicMetre = Value<-1, 1, -2, 0, 0, 0, 0>; // J/m3
pub type VoltPerMetre = Value<1, 1, -3, -1, 0, 0, 0>; // V/m
pub type CoulombPerCubicMetre = Value<-3, 0, 1, 1, 0, 0, 0>; // C/m3
pub type CoulombPerSquareMetre = Value<-2, 0, 1, 1, 0, 0, 0>; // C/m2
pub type FaradPerMetre = Value<-3, -1, 4, 2, 0, 0, 0>; // F/m
pub type HenryPerMetre = Value<1, 1, -2, -2, 0, 0, 0>; // H/m
pub type JoulePerMole = Value<2, 1, -2, 0, 0, -1, 0>; // J/mol
pub type JoulePerMoleKelvin = Value<2, 1, -2, 0, -1, -1, 0>; // J/(mol·K)
pub type CoulombPerKilogram = Value<0, -1, 1, 1, 0, 0, 0>; // C/kg
pub type GrayPerSecond = Value<2, 0, -3, 0, 0, 0, 0>; // Gy/s
pub type WattPerSteradian = Value<2, 1, -3, 0, 0, 0, 0>; // W/sr
pub type WattPerSquareMetreSteradian = Value<0, 1, -3, 0, 0, 0, 0>; // W/(m2·sr)
pub type KatalPerCubicMetre = Value<-3, 0, -1, 0, 0, 1, 0>; // kat/m3
pub type KelvinPerWatt = Value<-2, -1, 3, 0, 1, 0, 0>; // K/W
pub type SiemensPerMetre = Value<-3, -1, 3, 2, 0, 0, 0>; // S/m
pub type OhmMetre = Value<3, 1, -3, -2, 0, 0, 0>; // Ohm*m
pub type LumenSecond = Value<0, 0, 1, 0, 0, 0, 1>;
pub type LumenPerSquareMetre = Value<-2, 0, 0, 0, 0, 0, 1>;
pub type LuxSecond = Value<-2, 0, 1, 0, 0, 0, 1>;
pub type LumenSecondPerCubicMetre = Value<-3, 0, 1, 0, 0, 0, 1>;
pub type LumenPerWatt = Value<-2, -1, 3, 0, 0, 0, 1>;

pub const METRE: Metre = Metre { value: 1.0 };
pub const KILOGRAM: Kilogram = Kilogram { value: 1.0 };
pub const SECOND: Second = Second { value: 1.0 };
pub const AMPERE: Ampere = Ampere { value: 1.0 };
pub const KELVIN: Kelvin = Kelvin { value: 1.0 };
pub const MOLE: Mole = Mole { value: 1.0 };
pub const CANDELA: Candela = Candela { value: 1.0 };
pub const HERTZ: Hertz = Hertz { value: 1.0 };
pub const RADIAN: Radian = Radian { value: 1.0 };
pub const STERADIAN: Steradian = Steradian { value: 1.0 };
pub const NEWTON: Newton = Newton { value: 1.0 };
pub const PASCAL: Pascal = Pascal { value: 1.0 };
pub const JOULE: Joule = Joule { value: 1.0 };
pub const WATT: Watt = Watt { value: 1.0 };
pub const COULOMB: Coulomb = Coulomb { value: 1.0 };
pub const VOLT: Volt = Volt { value: 1.0 };
pub const FARAD: Farad = Farad { value: 1.0 };
pub const OHM: Ohm = Ohm { value: 1.0 };
pub const SIEMENS: Siemens = Siemens { value: 1.0 };
pub const WEBER: Weber = Weber { value: 1.0 };
pub const TESLA: Tesla = Tesla { value: 1.0 };
pub const HENRY: Henry = Henry { value: 1.0 };
pub const LUMEN: Lumen = Lumen { value: 1.0 };
pub const LUX: Lux = Lux { value: 1.0 };
pub const BECQUEREL: Becquerel = Becquerel { value: 1.0 };
pub const GRAY: Gray = Gray { value: 1.0 };
pub const SIEVERT: Sievert = Sievert { value: 1.0 };
pub const KATAL: Katal = Katal { value: 1.0 };

pub const EXA: f64 = 1_000_000_000_000_000_000.0;
pub const PETA: f64 = 1_000_000_000_000_000.0;
pub const TERA: f64 = 1_000_000_000_000.0;
pub const GIGA: f64 = 1_000_000_000.0;
pub const MEGA: f64 = 1_000_000.0;
pub const KILO: f64 = 1_000.0;
pub const HECTO: f64 = 100.0;
pub const DECA: f64 = 10.0;
pub const DECI: f64 = 0.1;
pub const CENT: f64 = 0.01;
pub const MILLI: f64 = 0.001;
pub const MICRO: f64 = 0.000_001;
pub const NANO: f64 = 0.000_000_001;
pub const PICO: f64 = 0.000_000_000_001;
pub const FEMTO: f64 = 0.000_000_000_000_001;
pub const ATTO: f64 = 0.000_000_000_000_000_001;

pub const HYPERFINE_TRANSITION_FREQUENCY_OF_CS: Hertz = Hertz {
    value: 9_192_631_770.0,
};
pub const SPEED_OF_LIGHT: MetrePerSecond = MetrePerSecond { value: 299792458.0 };
pub const PLANCK_CONSTANT: NewtonMetreSecond = NewtonMetreSecond {
    value: 6.62607015e-34,
};
pub const ELEMENTARY_CHARGE: Coulomb = Coulomb {
    value: 1.602176634e-19,
};
pub const BOLTZMANN_CONSTANT: JoulePerKelvin = JoulePerKelvin {
    value: 1.380649e-23,
};
pub const AVOGADRO_CONSTANT: ReciprocalMole = ReciprocalMole {
    value: 6.02214076e23,
};
pub const LUMINOUS_EFFICACY_OF_540_THZ_RADIATION: LumenPerWatt = LumenPerWatt { value: 683.0 };

#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Minute {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Hour {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Day {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Inch {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Foot {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Yard {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Mile {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct USSurveyFoot {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct USSurveyMile {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct NauticalMile {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Gram {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Ounce {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Pound {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Celsius {
    value: f64,
}
#[derive(Debug, Default, Copy, Clone, PartialEq, PartialOrd)]
pub struct Fahrenheit {
    value: f64,
}

pub trait HasConversionFactor {
    type OutUnitType;
    const FACTOR: f64;
}

impl HasConversionFactor for Minute {
    type OutUnitType = Second;
    const FACTOR: f64 = 60.0;
}
impl HasConversionFactor for Hour {
    type OutUnitType = Second;
    const FACTOR: f64 = 60.0 * 60.0;
}
impl HasConversionFactor for Day {
    type OutUnitType = Second;
    const FACTOR: f64 = 60.0 * 60.0 * 24.0;
}
impl HasConversionFactor for Inch {
    type OutUnitType = Metre;
    const FACTOR: f64 = 0.0254;
}
impl HasConversionFactor for Foot {
    type OutUnitType = Metre;
    const FACTOR: f64 = 0.3048;
}
impl HasConversionFactor for Yard {
    type OutUnitType = Metre;
    const FACTOR: f64 = 0.9144;
}
impl HasConversionFactor for Mile {
    type OutUnitType = Metre;
    const FACTOR: f64 = 1609.344;
}
impl HasConversionFactor for USSurveyFoot {
    type OutUnitType = Metre;
    const FACTOR: f64 = 0.30480061;
}
impl HasConversionFactor for USSurveyMile {
    type OutUnitType = Metre;
    const FACTOR: f64 = 1609.347218694;
}
impl HasConversionFactor for NauticalMile {
    type OutUnitType = Metre;
    const FACTOR: f64 = 1852.0;
}
impl HasConversionFactor for Gram {
    type OutUnitType = Kilogram;
    const FACTOR: f64 = 0.001;
}
impl HasConversionFactor for Ounce {
    type OutUnitType = Kilogram;
    const FACTOR: f64 = 0.02834952;
}
impl HasConversionFactor for Pound {
    type OutUnitType = Kilogram;
    const FACTOR: f64 = 0.45359237;
}

macro_rules! codegen_conversion_functions {
    ($(($T:ident, $to:ident, $from:ident)),*) => {
        $(
            pub fn $to(x: <$T as HasConversionFactor>::OutUnitType) -> $T {
                $T{value: x.value / <$T>::FACTOR}
            }
            pub fn $from(x: $T) -> <$T as HasConversionFactor>::OutUnitType {
                <$T as HasConversionFactor>::OutUnitType{value: x.value * <$T>::FACTOR}
            }

        )*
    }
}
codegen_conversion_functions!(
    (Minute, to_minute, from_minute),
    (Hour, to_hour, from_hour),
    (Day, to_day, from_day),
    (Inch, to_inch, from_inch),
    (Foot, to_foot, from_foot),
    (Yard, to_yard, from_yard),
    (Mile, to_mile, from_mile),
    (USSurveyFoot, to_us_survey_foot, from_us_survey_foot),
    (USSurveyMile, to_us_survey_mile, from_us_survey_mile),
    (NauticalMile, to_nautical_mile, from_nautical_mile),
    (Gram, to_gram, from_gram),
    (Ounce, to_ounce, from_ounce),
    (Pound, to_pound, from_pound)
);

pub fn from_fahrenheit(fahrenheit: Fahrenheit) -> Kelvin {
    Kelvin {
        value: 5.0 / 9.0 * (fahrenheit.value + 459.67),
    }
}
pub fn to_fahrenheit(kelvin: Kelvin) -> Fahrenheit {
    Fahrenheit {
        value: 5.0 / 9.0 * kelvin.value - 459.67,
    }
}

pub fn from_celsius(celsius: Celsius) -> Kelvin {
    Kelvin {
        value: celsius.value + 273.15,
    }
}
pub fn to_celsius(kelvin: Kelvin) -> Celsius {
    Celsius {
        value: kelvin.value - 273.15,
    }
}

fn avg_force(ds: Metre, dt: Second, m: Kilogram) -> Newton {
    let dv = ds / dt;
    let a = dv / dt;
    1.0 / (1.0 / (m * a))
}

fn main() {
    let a = Metre { value: 2.0 };
    let b = Metre { value: -1.0 };
    let x = Tesla { value: -1.0 };
    let c = a + b;
    let d = c - b;
    let e: Value<2, 0, 0, 0, 0, 0, 0> = a * b;
    let f = e * x;
    let mut g = a / f;
    g += Scalar { value: 2.0 } * g;
    let t = d == b;
    let h = 2.0 * Metre { value: 1.0 };
    let i = 3.0 * h * 3.0;
    let j = 3.0 / Metre { value: 4.0 } / 3.0;
    let mut k = 5.0 * METRE;
    k -= 2.0 * k;
    k *= 3.0;
    k /= 2.0;
    k = -k;
    println!("Hi {a} {b}, {c}, {d}, {e}, {t}, {f}, {g}, {h}, {i}, {j}. {k}");

    let dist_f = Foot { value: 300.0 };
    let dist = from_foot(dist_f);
    let time = Second { value: 12.5 };
    let mass = Kilogram { value: 800.0 };

    println!(
        "Force on body with mass {m} that moves {d} in {t}: {f}",
        d = dist,
        t = time,
        m = mass,
        f = avg_force(dist, time, mass)
    );

    println!("hyperfine transition frequency of Cs = {HYPERFINE_TRANSITION_FREQUENCY_OF_CS}");
    println!("speed of light = {SPEED_OF_LIGHT}");
    println!("Planck constant = {PLANCK_CONSTANT}");
    println!("elementary charge = {ELEMENTARY_CHARGE}");
    println!("Boltzmann constant = {BOLTZMANN_CONSTANT}");
    println!("Avogadro constant = {AVOGADRO_CONSTANT}");
    println!("luminous efficacy of 540 THz radiation = {LUMINOUS_EFFICACY_OF_540_THZ_RADIATION}");

    println!("LumenSecond = {};", get_dims_string(LUMEN * SECOND));
}
